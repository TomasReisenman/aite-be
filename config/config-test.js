const apartments = [
  {
    id: 1,
    address: 'John Doe',
    description: 'Amazing',
    rooms: 3,
    rentPrice: 500,
    state: "libre",
    university: "UBA"
  },
  {
    id: 2,
    address: 'Bob Williams',
    description: 'Great',
    rooms: 3,
    rentPrice: 500,
    state: "Buscando",
    university: "UBA"
  },
  {
    id: 3,
    address: 'Shannon Jackson',
    description: 'OK',
    rooms: 3,
    rentPrice: 500,
    state: "libre",
    university: "UBA"

  }
];

const petitions = [
  {
    id: 1,
    contact: '59909',
    rooms: 3,
    budget: 500,
    state: "Buscando",
    university: "UBA"
  },
  {
    id: 2,
    contact: '59909',
    rooms: 2,
    budget: 700,
    state: "Buscando",
    university: "UBA"
  },
  {
    id: 3,
    contact: '59909',
    rooms: 3,
    budget: 900,
    state: "Buscando",
    university: "UBA"
  }
];

module.exports = {
  apartments,
  petitions
};