const multer = require('multer');
const csv = require('csv-parser');  
const fs = require('fs');


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './tmp')
    },
    filename: function (req, file, cb) {
        cb(null, createNameForFile(file.originalname) )
    }
})

const upload = multer({ storage: storage });


async function getRows(filePath){

    return new Promise(function (resolve,reject){
 
    const rows = [];

     fs.createReadStream(filePath)  
    .pipe(csv())
    .on('data', (row) => {
        console.log(row)
      rows.push(row)
    })
    .on('end', () => {
        console.log('CSV file successfully processed');
        resolve(rows);
    });

});

}

function createNameForFile(originalName){
    
    let now = new Date();
    
    return  now.getDate() + '-' +
            (now.getMonth() +1) + '-' +
            now.getFullYear() + '-' +
            now.getHours() + '-' +
            now.getMinutes() + '-' +
            now.getMilliseconds() + '-' +
            originalName;
}

module.exports = {
    getRows,
    upload
}