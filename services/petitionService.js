const petitionRepo = require('../repositories/testPetitionRepo');
const apartmentRepo = require('../repositories/testRepo');

function isAMatch(petition, apartment){
    if(petition.rooms <= apartment.rooms && 
        petition.budget >= apartment.rentPrice &&
        apartment.state === 'libre' &&
        petition.university === apartment.university){
            return true;

        }else {
            return false
        }
}

async function getAllMatches (petition){

    const apartments = await apartmentRepo.getAll();
    return apartments.filter(apartment => isAMatch(petition,apartment));

}

function getPetitionById(id){

    
    return petitionRepo.getById(id);
}



module.exports = {
    getAllMatches,
    getPetitionById
}

