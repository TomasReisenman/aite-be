const db = require('../config/config-test').apartments;

function getAll(){

    return db;
}

function add(newMember){

    return db.push(newMember);
}


function getById(id){

    return db.find( apartment => apartment.id === id);
}


function deleteById(id){


    let index = db.map(x => {
        return x.id;
      }).indexOf(id);

    return db.splice(index,1);
}

function updateById(id, update){

    const toUpdate = getById(id);

    toUpdate.address = update.address ? update.address : toUpdate.address;
    toUpdate.description = update.description ? update.description : toUpdate.description;

    return toUpdate
}

module.exports = {
    getAll,
    add,
    getById,
    deleteById,
    updateById
}