const db = require('../config/config-local');

async function getAll(){

    return db.select().from('APARTMENTS');
}

function add(newMember){

    return db('APARTMENTS').insert(newMember);
}


function getById(id){

    return db.select().from('APARTMENTS').whereIn('id',[id]).first();
}


function deleteById(id){

    return db('APARTMENTS')
    .where('id', id)
    .del()
}

function updateById(id, update){

    return db('APARTMENTS').where({ id: id }).update(update).returning('*');

    
}

module.exports = {
    getAll,
    add,
    getById,
    deleteById,
    updateById
}