const express = require('express');
const cors = require('cors');
const app = express();
const PORT = 5000;

app.use(express.json());

app.use(cors());

app.use('/apartments', require('./routes/apartments'));

app.use('/data', require('./routes/data'));

app.listen(PORT, () => console.log(`Server started on port ${PORT}`))