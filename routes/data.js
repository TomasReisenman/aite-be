const express = require('express');
const router = express.Router();
const fileService = require('../services/fileService')

const upload = fileService.upload;

router.post('/', upload.single('myFile'), async (req, res) => {
    
    let file = req.file;

    const rows = await fileService.getRows(file.path) ;

    res.status(200);
    res.json(rows);

})

module.exports = router;