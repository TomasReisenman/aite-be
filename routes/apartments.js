const express = require('express');
const router = express.Router();
const uuid = require('uuid');
const apartmentRepo = require('../repositories/testRepo');
const petitionService  = require('../services/petitionService');



router.get('/', async (req, res) => {
    res.status(201);
    res.json(await apartmentRepo.getAll());
})

router.post('/', async (req, res) => {
    
    const newAppartment = req.body;

    newAppartment.id = uuid.v4();

    await apartmentRepo.add(newAppartment);
    res.status(201);
    res.json(newAppartment);
    ;
});

router.get('/:id', async (req, res) => {

    const found = await apartmentRepo.getById(parseInt(req.params.id));

    if (found) {

        res.status(200);
        res.send(found);

    } else {
        res.status(400).json({ msg: `No apartment with the id of ${req.params.id}` });
    }

});


router.delete('/:id', async (req, res) => {
    const found = await apartmentRepo.getById(parseInt(req.params.id));

    if (found) {
        await apartmentRepo.deleteById(parseInt(req.params.id));
        res.json({
            msg: 'Apartment deleted',
            apartments: await apartmentRepo.getAll()
        });
    } else {
        res.status(400).json({ msg: `No apartment with the id of ${req.params.id}` });
    }
});

router.put('/:id', async (req, res) => {
    const found = await apartmentRepo.getById(parseInt(req.params.id));

    if (found) {
        const toUpdate = req.body;
        const updated = await apartmentRepo.updateById(parseInt(req.params.id), toUpdate)
        res.json({
            msg: 'Apartment updated',
            apartment: updated
        });

    } else {
        res.status(400).json({ msg: `No member with the id of ${req.params.id}` });
    }
});

router.get('/match/:id', async (req, res) => {
    const found = await petitionService.getPetitionById(parseInt(req.params.id));
    let apartments ;   
    
    try {
        
        if (found) {
            apartments = await petitionService.getAllMatches(found);

        } else {
            throw { status: 404, descripcion: `No member with the id of ${req.params.id}` }
        }

        res.status(200).json({
            msg: 'Apartamentos encontrados',
            apartments: apartments
        });

    } catch (err) {

        res.status(err.status).json(err)

    }

});



module.exports = router;