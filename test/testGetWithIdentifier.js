const request = require('request-promise-native')

module.exports = async function testGetWithIdentifier(serverUrl) {

    const targetId = 1

    const options = {
        uri: serverUrl + targetId,
        json: true
    }

    try {
        const apartment = await request(options)

        if (!apartment)
            throw "get by id: apartment vacío (sin apartment)"

        if (!apartment.hasOwnProperty('id'))
            throw "get by id: el apartment recibido no tiene id"

        if (apartment.id != targetId)
            throw "get by id: el apartment recibido no es el buscado"

        if (!apartment.hasOwnProperty('address'))
            throw "get by id: el apartment recibido no tiene address"

        if (!apartment.hasOwnProperty('rentPrice'))
            throw "get by id: el apartment recibido no tiene rentPrice"

        if (!apartment.hasOwnProperty('university'))
            throw "get by id: el apartment recibido no tiene university"

        if (!apartment.hasOwnProperty('state'))
            throw "el apartment recibido no tiene state"

        console.log("get by id: ok")

    } catch (err) {
        if (err.status == 404)
            console.log("get by id: ok (not found)")
        else
            console.log(err)
    }
}
