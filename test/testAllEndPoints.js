const testGetAll = require('./testGetAll')
const testMatcher = require('./testMacher');
const testGetWithQueryParams = require('./testGetWithQueryParams')
const testGetWithIdentifier = require('./testGetWithIdentifier')
const testPostWithBody = require('./testPostWithBody')
const testDeleteWithIdentifier = require('./testDeleteWithIdentifier')
const testPutWithIdentifier = require('./testPutWithIdentifier')

const serverUrl = 'http://127.0.0.1:5000/apartments/'

async function main(){
    await testPostWithBody(serverUrl)
    await testGetAll(serverUrl)
    await testMatcher(serverUrl)
    await testGetWithIdentifier(serverUrl)
    //await testGetWithQueryParams(serverUrl)
    //await testPutWithIdentifier(serverUrl)
    await testDeleteWithIdentifier(serverUrl)
}

main()
