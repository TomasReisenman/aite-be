
const request = require('request-promise-native');

module.exports = async function testPutWithIdentifier(serverUrl) {

    const targetId = 0

    const nuevoEstu = {
        id: 0,
        nombre: 'Mirtha',
        apellido: 'Legrand',
        edad: 99,
        dni: '1'
    }

    const options = {
        method: 'PUT',
        uri: serverUrl + 'estudiantes' + '/' + targetId,
        body: nuevoEstu,
        json: true
    };

    try {
        const estudiante = await request(options)

        if (!estudiante)
            throw "put: mensaje vacío (sin estudiante)"

        if (!estudiante.hasOwnProperty('id'))
            throw "put: el estudiante recibido no tiene id"

        if (estudiante.id != targetId)
            throw "put: el estudiante recibido no es el reemplazado"

        if (!estudiante.hasOwnProperty('nombre'))
            throw "put: el estudiante recibido no tiene nombre"

        if (!estudiante.hasOwnProperty('apellido'))
            throw "put: el estudiante recibido no tiene apellido"

        if (!estudiante.hasOwnProperty('edad'))
            throw "put: el estudiante recibido no tiene edad"

        if (!estudiante.hasOwnProperty('dni'))
            throw "put: el estudiante recibido no tiene dni"

        console.log("put: ok")

    } catch (err) {
        console.log(err)
    }
}
