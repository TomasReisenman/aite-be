const request = require('request-promise-native');

module.exports = async function testGetAll(serverUrl) {

    const options = {
        uri: serverUrl,
        json: true
    }

    try {
        const apartments = await request(options)

        for (const apartment of apartments) {
            
            if (!apartment.hasOwnProperty('id'))
                throw "get all: apartment no tiene id"

            if (!apartment.hasOwnProperty('address'))
                throw "get all: apartment no tiene address"

            if (!apartment.hasOwnProperty('description'))
                throw "get all: apartment no tiene descripcion"
        }

        console.log("get all: ok")

    } catch (err) {
        console.log(err)
    }
}
