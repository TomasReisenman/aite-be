const request = require('request-promise-native');

module.exports = async function testPostWithBody(serverUrl) {

    const newApartment = {
        
        address: 'LLatai',
        description: 'Great',
        rooms: 5,
        rentPrice: 500,
        state: "Buscando",
        university: "ORT"
      }

    const options = {
        method: 'POST',
        uri: serverUrl,
        body: newApartment,
        json: true
    }

    try {
        const apartment = await request(options)

        if (!apartment)
            throw "get by id: apartment vacío (sin apartment)"

        if (!apartment.hasOwnProperty('id'))
            throw "get by id: el apartment recibido no tiene id"

        if (!apartment.hasOwnProperty('address'))
            throw "get by id: el apartment recibido no tiene address"

        if (!apartment.hasOwnProperty('rentPrice'))
            throw "get by id: el apartment recibido no tiene rentPrice"

        if (!apartment.hasOwnProperty('university'))
            throw "get by id: el apartment recibido no tiene university"

        if (!apartment.hasOwnProperty('state'))
            throw "el apartment recibido no tiene state"

        console.log("post: ok")

    } catch (err) {
        console.log(err)
    }
}
