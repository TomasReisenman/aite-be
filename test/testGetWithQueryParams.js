const request = require('request-promise-native');

module.exports = async function testGetWithQueryParams(serverUrl){

    const EDAD_MIN = 2
    const EDAD_MAX = 4

    const options = {
        uri: serverUrl + 'estudiantes',
        qs: { edadMin: EDAD_MIN, edadMax: EDAD_MAX },
        json: true
    }
    
    try {
        const estudiantes = await request(options)

        for (const estudiante of estudiantes) {
            
            if (!estudiante.id)
                throw "get with query: el estudiante recibido no tiene id"

            if (!estudiante.nombre)
                throw "get with query: el estudiante recibido no tiene nombre"

            if (!estudiante.apellido)
                throw "get with query: el estudiante recibido no tiene apellido"

            if (!estudiante.edad)
                throw "get with query: el estudiante recibido no tiene edad"

            if (estudiante.edad < EDAD_MIN || estudiante.edad > EDAD_MAX)
                throw "get with query: el estudiante recibido tiene edad fuera de rango"

            if (!estudiante.dni)
                throw "get with query: el estudiante recibido no tiene dni"
        }

        console.log("get by query: ok")

    } catch (err) {
        console.log(err)
    }
}
