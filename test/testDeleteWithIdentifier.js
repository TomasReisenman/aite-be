const request = require('request-promise-native');
const db = require('../config/config-test').petitions;

module.exports = async function testDeleteWithIdentifier(serverUrl) {

    const targetId = 2
    
    let expectedResult = [];

    expectedResult.push(db[0],db[2]);

    const options = {
        method: 'DELETE',
        uri: serverUrl + '/' + targetId,
        json: true
    }

    try {
        const body = await request(options)
        
        if(body.apartments[0].id === expectedResult[0].id && 
            body.apartments[1].id === expectedResult[1].id){
            
            console.log('delete: ok')
        }else{
            throw 'Unexpected Result Get Macher'
        }
        
    
    } catch (err) {
        console.log(err)
    }
}
