const request = require('request-promise-native');
const db = require('../config/config-test').petitions;

module.exports = async function testMatcher(serverUrl) {

    const targetId = 1

    let expectedResult = [];

    expectedResult.push(db[0],db[2]);

    const options = {
        method: 'GET',
        uri: serverUrl + '/match' + '/' + targetId,
        json: true
    }

    try {
        const body = await request(options)
        
        if(body.apartments[0].id === expectedResult[0].id && 
            body.apartments[1].id === expectedResult[1].id){
            
            console.log('get match: ok')
        }else{
            throw 'Unexpected Result Get Macher'
        }
   
    } catch (err) {
        console.log(err)
    }
}
